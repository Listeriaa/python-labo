'''
Les opérations sur un laboratoire
'''
class LaboException(Exception):
    """ Généralise les exceptions du laboratoire."""
    pass


class AbsentException(LaboException):
    pass


class PresentException(LaboException):
    pass

def Laboratoire() :
    return {}

def enregistrer_arrivee(labo, nom, bureau):
    '''
    enregistre le nom et le bureau d'un nouvel arrivé
    '''
    if nom in labo :
        raise PresentException
        
    labo[nom] = bureau

def enregistrer_depart(labo, nom) :
    '''
    supprime le nom et le bureau d'une personne partie
    '''
    if nom not in labo :
        raise AbsentException
    
    del labo[nom]

def modifier_bureau(labo, nom, bureau) :
    '''
    modifie le bureau d'une personne déjà enregistrée
    '''
    if nom not in labo :
        raise AbsentException

    labo[nom] = bureau

def modifier_identite(labo, ancien_nom, nv_nom) :
    '''
    modifie le nom d'une personne déjà enregistrée
    '''
    if ancien_nom not in labo :
        raise AbsentException
    
    bureau = labo[ancien_nom]
    del labo[ancien_nom]
    labo[nv_nom] = bureau

def deja_present(labo, nom):
    '''
    retourne False si la personne n'est pas enregistrée dans le labo, True sinon
    '''
    return nom in labo

def obtenir_bureau(labo, nom):
    '''
    obtenir le bureau d'une personne enregistrée à partir de son nom
    '''
    if nom not in labo :
        raise AbsentException
    
    return labo[nom]

def classer_par_bureau(labo):
    '''
    retourne le listing du laboratoire trié par bureau()
    '''
    listing = {}
    # Création du classement par bureau
    for nom, bureau in labo.items() :
        listing.setdefault(bureau, set())# si m est déjà dans index, le rajoute, sinon, le crée
        listing[bureau].add(nom)
    
    # tri de la clé et du set
    sorted_listing = {}
    for key in sorted(listing.keys()):
        sorted_listing[key] = sorted(listing[key])
    return sorted_listing