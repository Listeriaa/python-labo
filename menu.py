import sys

def Menu() :
    '''
    Crée une liste représentant le menu
    '''
    return []

def ajouter_texte(menu, texte, action, *args):
    '''
    ajouter au menu un tuple représentant le texte à afficher, le sous-programme associé à lancer et les éventuels arguments pour le sous-programme
    '''
    menu.append((texte, action, args))


def afficher_menu(menu):
    '''
    affiche le menu
    '''
    for num, (texte,_, _) in enumerate(menu, 1) :
        print(f"{num:2}- {texte}")
    print(f"{0:2}- Quitter")

def choix_utilisateur(menu):
    '''
    demande le choix de l'utilisateur
    '''
    saisie = input_int("Indiquez votre choix :")

    while saisie > len(menu) :
        print(f"Veuillez faire votre choix (de 1 à {len(menu)})")
        saisie = input_int("Indiquez votre choix :")
    return saisie

def traiter(menu, choix):
    '''
    lance l'action correspondant au choix de l'utilisateur
    '''
    if choix != 0 :
        _, action, args = menu[choix - 1]
        action(*args)

   

def lancer_application(menu):
    '''
    
    '''
    
    afficher_menu(menu)
    choix = choix_utilisateur(menu)
    while choix != 0 :
        traiter(menu, choix)
        choix = choix_utilisateur(menu)

    else : 
        sys.exit()
    
def input_int(consigne):
    '''
    Retourner un entier saisi au clavier.  Si l'utilisateur ne donne pas un
    entier, un message d'erreur est affiché et un entier est demandé à nouveau.
    Avant chaque sollicitation de l'utilisateur, la consigne est affichée.

    '''
    saisie_ok = False
    while not saisie_ok:
        reponse = input(consigne)
        reponse = reponse.strip()
        saisie_ok = reponse.isdecimal()
        if not saisie_ok:
            print('Il faut saisir un entier.')
    return int(reponse)