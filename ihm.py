
import laboratoire
import menu


def gerer_arrivee(labo):
    '''
    Enregistre l'arrivée d'une nouvelle personne au laboratoire
    '''
    nom = input("Nom? ").strip()
    if not laboratoire.deja_present(labo, nom) :

        bureau=input("Bureau? ").strip()
  
        laboratoire.enregistrer_arrivee(labo, nom, bureau)
        afficher_listing(labo)
    else:
        print("Cette personne est déjà enregistrée")


def gerer_depart(labo) :
    '''
    Supprime une personne du listing du laboratoire
    '''
    try:
        nom = input("Nom? ").strip()

        laboratoire.enregistrer_depart(labo, nom)
        print(f"{nom} a bien été supprimée du labo")
        afficher_listing(labo)

    except laboratoire.AbsentException:
        print("Cette personne n'est pas enregistrée")


def gerer_bureau(labo):
    '''
    Modifie le bureau d'une personne enregistrée dans le laboratoire
    '''
    nom = input("Nom? ").strip()
    if laboratoire.deja_present(labo, nom) :

        bureau = input("Nouveau bureau? ").strip()
  
        laboratoire.modifier_bureau(labo, nom, bureau)
        afficher_listing(labo)

    else:
        print("Cette personne n'est pas enregistrée")


def gerer_identite(labo):
    '''
    Modifie le nom d'une personne enregistrée dans le laboratoire
    '''
    ancien_nom = input("Personne à modifier? ").strip()

    if laboratoire.deja_present(labo, ancien_nom) :
        nv_nom = input("Nouvelle identité :").strip()
        laboratoire.modifier_identite(labo, ancien_nom, nv_nom)
        afficher_listing(labo)
    else :
        print("Cette personne n'est pas enregistrée")


def connaitre_bureau(labo):
    '''
    affiche le bureau d'une personne
    '''

    nom = input("Nom? ").strip()
    try :
        bureau = laboratoire.obtenir_bureau(labo, nom)
        print(f"Le bureau de {nom:10f} est : {bureau:5f}")

    except laboratoire.AbsentException: 
        print("Cette personne n'est pas enregistrée")
    
def avoir_listing(labo):
    '''
    Affiche le listing du personnel par nom
    '''
    
    for nom in sorted(labo.keys()):
        print(f"- {nom:10} est dans le bureau {labo[nom]:5}")
    

def avoir_listing_inverse(labo):
    '''
    Affiche le listing du personnel par bureau
    '''

    listing = laboratoire.classer_par_bureau(labo)

    for bureau, noms in listing.items() :
        print(f"Bureau {bureau } :")
        for nom in noms :
            print(f"- {nom}")

def listing_HTML(labo):
    listing = laboratoire.classer_par_bureau(labo)


    print('''
    <!DOCTYPE html>
    <html lang="fr">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Laboratoire</title>
        </head>
        <body>
            <h1> Listing du personnel par bureau </h1>
            <ul>''')
    for bureau, noms in listing.items() :

        print(f"<li>Bureau {bureau } : ")
        print("<ul>")
        for nom in noms :
            print(f"<li> {nom} </li>")    

    print('''</ul>
            </li>
            </ul>
        </body>
        </html>
        ''')


def afficher_listing(labo) :
    '''
    affiche le listing brut après une action de modification
    '''
    print("Laboratoire : ", labo)



def main() :
    choix = None
    labo = laboratoire.Laboratoire()

    mp = menu.Menu()

    menu.ajouter_texte(mp, "Enregistrer une arrivée", gerer_arrivee, labo)
    menu.ajouter_texte(mp, "Enregistrer un départ", gerer_depart, labo)
    menu.ajouter_texte(mp, "Modifier le bureau", gerer_bureau, labo)
    menu.ajouter_texte(mp, "Modifier l'identité d'une personne", gerer_identite, labo)
    menu.ajouter_texte(mp, "Connaître le bureau d'une personne", connaitre_bureau, labo)
    menu.ajouter_texte(mp, "Avoir le listing du personnel", avoir_listing, labo)
    menu.ajouter_texte(mp, "Avoir le listing du personnel par bureau", avoir_listing_inverse, labo)
    menu.ajouter_texte(mp, "Listing par bureau HTML", listing_HTML, labo)
    

    menu.lancer_application(mp)
  



if __name__ == "__main__":
    main()