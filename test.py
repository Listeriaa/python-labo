import laboratoire
import menu
import pytest

def test_arrivee() :

    labo = {"tom" : "Bureau1"}

    laboratoire.enregistrer_arrivee(labo, "ben", "bureau2")

    assert len(labo) == 2

    with pytest.raises(laboratoire.PresentException) :
        laboratoire.enregistrer_arrivee(labo, "tom", "bureau2")

def test_depart() :

    labo = {"tom" : "Bureau1"}

    laboratoire.enregistrer_depart(labo, "tom")

    assert len(labo) == 0

    with pytest.raises(laboratoire.AbsentException) :
        laboratoire.enregistrer_depart(labo, "ben")

def test_modifier_bureau():

    labo = {"tom" : "Bureau1"}
    laboratoire.modifier_bureau(labo, "tom", "Bureau100")

    assert labo["tom"] == "Bureau100"

    with pytest.raises(laboratoire.AbsentException) :
        laboratoire.modifier_bureau(labo, "ben", "bureau2")

def test_modifier_identite():

    labo = {"tom" : "Bureau1"}

    laboratoire.modifier_identite(labo, "tom", "laeti")

    assert labo.get("laeti") != None
    assert labo.get("tom") == None

    with pytest.raises(laboratoire.AbsentException) :
        laboratoire.modifier_identite(labo, "ben", "bureau2")

def test_present() :
    labo = {"tom" : "Bureau1"}

    assert laboratoire.deja_present(labo, "tom") == True

    assert laboratoire.deja_present(labo, "laeti") == False


def test_obtenir_bureau() :

    labo = {"tom" : "Bureau1"}

    assert laboratoire.obtenir_bureau(labo, "tom") == "Bureau1"

    with pytest.raises(laboratoire.AbsentException) :
        laboratoire.obtenir_bureau(labo, "ben")
